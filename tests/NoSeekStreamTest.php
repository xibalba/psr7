<?php
namespace xibalba\tests\psr7;

use xibalba\psr7\Psr7;
use xibalba\psr7\NoSeekStream;

/**
 * @covers xibalba\psr7\NoSeekStream
 * @covers xibalba\psr7\StreamDecoratorTrait
 */
class NoSeekStreamTest extends \PHPUnit_Framework_TestCase {
	/**
	 * @expectedException \RuntimeException
	 * @expectedExceptionMessage Cannot seek a NoSeekStream
	 */
	public function testCannotSeek() {
		$s = $this->getMockBuilder('Psr\Http\Message\StreamInterface')
			->setMethods(['isSeekable', 'seek'])
			->getMockForAbstractClass();
		$s->expects($this->never())->method('seek');
		$s->expects($this->never())->method('isSeekable');
		$wrapped = new NoSeekStream($s);
		$this->assertFalse($wrapped->isSeekable());
		$wrapped->seek(2);
	}

	/**
	 * @expectedException \RuntimeException
	 * @expectedExceptionMessage Cannot write to a non-writable stream
	 */
	public function testHandlesClose() {
		$s = Psr7::streamFor('foo');
		$wrapped = new NoSeekStream($s);
		$wrapped->close();
		$wrapped->write('foo');
	}
}
